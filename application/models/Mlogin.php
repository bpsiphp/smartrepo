<?php 
class Mlogin extends CI_Model
{
	public function searchUser($LoginOption,$table,$FieldPassword,$Password,$FieldEmail,$EmailID,$FieldEmployeeID,$EmployeeID,$FieldOrg,$OrganizationID,$FieldPhoneNumber,$PhoneNumber,$FieldCountryCode,$CountryCode){		
		if($table=='tbl_sponsorer'){
			$sel='id';			
		}else{
			$sel='*';
		}
		$q="select ".$sel." from ".$table." where "; 			
			if($EmailID!="" && $LoginOption=='EmailID'){
				$q.=$FieldEmail."='".$EmailID."' and ".$FieldPassword."='".$Password."'";
			}
			
			if($EmployeeID!="" && $LoginOption=='EmployeeID'){
				$q.=$FieldEmployeeID."='".$EmployeeID."' and ".$FieldOrg."='".$OrganizationID."' and ".$FieldPassword."='".$Password."'";
			}
			
			if($PhoneNumber!="" && $LoginOption=='PhoneNumber' ){
				if($table=='tbl_sponsorer'){
					if($FieldCountryCode!=""){					
						$q.="(".$FieldPhoneNumber."='".$PhoneNumber."' or sp_landline='".$PhoneNumber."' ) and ".$FieldCountryCode."='".$CountryCode."' and ".$FieldPassword."='".$Password."'";	
					}else{
						$q.="(".$FieldPhoneNumber."='".$PhoneNumber."' or sp_landline='".$PhoneNumber."' ) and ".$FieldPassword."='".$Password."'";
					}
				}else{
					if($FieldCountryCode!=""){					
						$q.=$FieldPhoneNumber."='".$PhoneNumber."' and ".$FieldCountryCode."='".$CountryCode."' and ".$FieldPassword."='".$Password."'";	
					}else{
						$q.=$FieldPhoneNumber."='".$PhoneNumber."' and ".$FieldPassword."='".$Password."'";
					}
				}
			}
			if($table=='tbl_sponsorer'){
				$q.=' order by sp_company ASC, sp_name ASC';
			}	
		
			$query=$this->db->query($q);	
			echo $q;
			$data['Result']=$query->result();
			$data['TotalUser']=$query->num_rows();
			
			return $data;			
	}
	
	public function errorMultipleUsers($entity,$record){
		$query=$this->db->query("insert into `tbl_error_log` (`id`, `error_type`, `error_description`, `data`, `datetime`, `user_type`, `last_programmer_name`) values(NULL, 'More Than 1 User', 'Login.php', '$record', CURRENT_TIMESTAMP, '$entity', 'Sudhir')");
		
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}
	
	public function setLoginLogoutStatus($TblEntityID, $UserID,$lat,$lon,$CountryCode,$ip,$os,$browser,$school_id){
		$date=date('Y-m-d H:i:s');
		$q=$this->db->get_where("tbl_LoginStatus","EntityID = '$UserID' AND Entity_type= '$TblEntityID'");
		$rows=$q->num_rows();
		if( $rows > 0 ){
			
			$updata=array(
				'LatestLoginTime'=>$date,
				'LatestMethod'=>'web',
				'LatestDevicetype'=>'',
				'LatestDeviceDetails'=>$os,
				'LatestPlatformOS'=>$os,
				'LatestIPAddress'=>$ip,
				'LatestLatitude'=>$lat,
				'LatestLongitude'=>$lon,
				'LatestBrowser'=>$browser,
				'school_id'=>$school_id	
			);			
		
			$q=$this->db->update("tbl_LoginStatus",$updata,"EntityID = '$UserID' AND Entity_type= '$TblEntityID'");		
		
		}else{
			$insdata=array(
					'EntityID'=>$UserID,
					'Entity_type'=>$TblEntityID,
					'FirstLoginTime'=>$date,
					'FirstMethod'=>'web',
					'FirstDevicetype'=>'',
					'FirstDeviceDetails'=>$os,
					'FirstPlatformOS'=>$os,
					'FirstIPAddress'=>$ip,
					'FirstLatitude'=>$lat,
					'FirstLongitude'=>$lon,
					'FirstBrowser'=>$browser,			
					'LatestLoginTime'=>$date,
					'LatestMethod'=>'web',
					'LatestDevicetype'=>'',
					'LatestDeviceDetails'=>$os,
					'LatestPlatformOS'=>$os,
					'LatestIPAddress'=>$ip,
					'LatestLatitude'=>$lat,
					'LatestLongitude'=>$lon,
					'LatestBrowser'=>$browser,			
					'CountryCode'=>$CountryCode,			
					'school_id'=>$school_id			
			);
		
		$q=$this->db->insert("tbl_LoginStatus",$insdata);
				}
		if($q){
			return true;
		}else{
			return false;
		}
	}
	public function sessionLogout($TblEntityID,$UserID)	{				
	$update=array(	
	
				'LogoutTime'=>date("Y-m-d h:i:s")	
			);		
				$up=$this->db->update("tbl_LoginStatus",$update,"EntityID='$UserID' AND Entity_type='$TblEntityID'");		
	}
}

?>