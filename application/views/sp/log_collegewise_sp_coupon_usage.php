<?php $this->load->view('sp/header'); ?>

<script>
$(document).ready(function(){
	
    $('#myTable').DataTable();
	
});
</script>

<div class="panel panel-violet">
		<div class="panel-heading">
			College Wise Sponsor Coupon Usage
		</div>
		<div class="panel-body">
	<div class="table-responsive" id="no-more-tables">
	<table class="table table-bordered table-striped table-condensed cf" id="myTable">
	<thead class="cf">
			<tr>
				<th colspan="1" rowspan="2">#</th>
				<th rowspan="2">School / College</th>
				<th rowspan="1" colspan="3">Student</th>
				<th rowspan="1" colspan="3">Teacher</th>
			</tr>
				<tr>
					<th>Total</th>
					<th>Used</th>
					<th>Unused</th>
					<th>Total</th>
					<th>Used</th>
					<th>Unused</th>
				</tr>
			</thead>
			<tbody>					
<?php 
$sr=1;

if($log_collegewise_sp_coupon_usage){
foreach ($log_collegewise_sp_coupon_usage as $key => $value): 
//print_r($value);
?>	
				<tr >
				<td data-title="SR." ><?=$sr;?></td>
				<td data-title="Institute" ><?=$value[0]->school;?><span class="badge"><?=$value[0]->school_count;?></span></td>
				<td data-title="Student Total" ><?=$value[0]->studs;?></td>
				<td data-title="Student Used" ><?=$value[0]->used_studs;?></td>
				<td data-title="Student Unused" ><?=$value[0]->unused_studs;?></td>
				<td data-title="Teacher Total" ><?=$value[0]->teachers;?></td>
				<td data-title="Teacher Used" ><?=$value[0]->used_teachers;?></td>
				<td data-title="Teacher Unused" ><?=$value[0]->unused_teachers;?></td>
				</tr>
<?php 
$sr++;
endforeach;
}
 ?>		

			</tbody>
	</table>
			</div>
		</div>
	</div>

