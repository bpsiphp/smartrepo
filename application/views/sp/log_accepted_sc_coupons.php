<?php $this->load->view('sp/header'); 
$this->load->helper('imageurl');
?>

<script>
$(document).ready(function(){
	
    $('#myTable').DataTable();
	
});
</script>

<div class="panel panel-violet">
  <div class="panel-heading">
  Accepted SmartCookie Coupons <span class="badge"><?=$count_accepted_sc_coupons;?></span>
  </div>
  <div class="panel-body">		
	<div class="table-responsive" id="no-more-tables">
	<table class="table table-bordered table-striped table-condensed cf" id="myTable">
	<thead class="cf">
	<tr>
	<th>#</th>
	<th>Image</th>
	<th>Used By</th>
	<th>User Type</th>
	<th>Code</th>
	<th>Product</th>
	<th>Points</th>
	<th>Date<br/>(DD/MM/YYYY)</th>
	</tr>
	</thead>
	<tbody>
<?php 

$sr=1;
foreach ($log_accepted_sc_coupons as $key => $value): 
//print_r($value);
		$vu1=explode('/',$value->issue_date);
		$vu=$vu1[1].'/'.$vu1[0].'/'.$vu1[2];
?>
<tr>
	<td data-title="Sr." ><?=$sr;?></td>	
	<td data-title="Image" ><img src="<?php echo imageurl($value->photo,'avatar');?>" height="64px" width="64px"></td>	
	<td data-title="Used By" ><?=$value->name;?></td>
	<td data-title="User Type" ><?=$value->user_type;?></td>
	<td data-title="Code" ><?=$value->coupon_id;?></td>
	<td data-title="Product" ><?=$value->product_name;?></td>	
	<td data-title="Points" ><?=$value->points;?></td>
	<td data-title="Date(DD/MM/YYYY)" ><?=$vu;?></td>	
</tr>
<?php 
$sr++;
endforeach;
 ?>		

</tbody>
</table></div></div>
	</div>
	
