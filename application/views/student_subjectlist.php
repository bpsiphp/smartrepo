<?php 
//print_r($schoolinfo);

$this->load->view('stud_header',$studentinfo);

?>

<!DOCTYPE html>
<html lang="en">
<head>
<script>
	
        $(document).ready(function() {
            $('#example').dataTable( {
		
				
         });
			
  
        } );
		
		
        </script>
</head>

<title>Subject List</title>
    

<body>

    <!--END THEME SETTING-->

   
        <!--END SIDEBAR MENU--><!--BEGIN CHAT FORM-->
   
     <div id="area-chart-spline" style="width: 100%; height:300px; display:none;"></div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
          
          
           
             <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">My Subjects</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="members">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Logs</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">My Subjects</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="generalTabContent" class="tab-content responsive" style="margin-top:4%;">
                            <div id="teacher" class="tab-pane fade in active">
                        
						<div class="row">
						   <?php 

echo form_open("main/student_subjectlist","class=form-horizontal");?>
						<div class="col-md-4">
						
						</div>
						<div class="col-md-4">
						<select id="select_opt" name="select_opt" class="form-control">
     <option value="1" <?php 
	 
	 if(isset($_POST['select_opt']))
	 {
		 if($_POST['select_opt']=="1")
		 { ?>
			 
			 selected 
	<?php } 
		 
	 }
	 ?>


	 ><?php echo "Current" ?></option>
	 <option value="2"   <?php 
	 
	 if(isset($_POST['select_opt']))
	 {
		 if($_POST['select_opt']=="2")
		 {?>
			 
			 selected 
	<?php } 
		 
	 }
	 ?>


	 ><?php echo "All" ?></option>
      </select><?php echo form_error('select_opt', '<div class="error">', '</div>'); ?>
						
						</div>
						<div class="col-md-4">
						  <?php echo form_submit('submit', 'Submit','class="btn btn-green"');
?>

						</div>
						<?php



echo form_close();

	?>
						
						
						</div>
						
						
						
						
                                <div class="row" style="margin-top:5%;">
                                
                                  <div id="no-more-tables">
                                    <table class="table table-bordered table-hover " id="example" >
                                        <thead class="cf">
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Subject Code</th>
                                            <th>Subject Name</th>
                                            <th>Semester</th>
                                             <th>Branch</th>
											 <th>Year</th>
                                            <th >Teacher Name</th>
                                           
                                            
                                           
                                           
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $i=1;
                                            foreach($student_subjectlist as $t) {?>
                                        <tr>
                                            <td data-title="Sr.No"><?php echo $i;?></td>
                                            <td data-title="Subject Code"><?php echo $t->subjcet_code ;?></td>
                                            <td data-title="Subject Name" ><?php echo $t->subjectName;?></td>
                                            <td data-title="Semester" ><?php echo $t->Semester_id ;?></td>
                                            <td data-title="Branch"><?php echo $t->Branches_id;?></td>
											 <td data-title="Branch"><?php echo $t->AcademicYear;?></td>
                                             <td data-title="Teacher Name"><?php echo ucwords(strtolower( $t->t_complete_name));?></td>
                                             
                                         
                                           
                                        </tr>
                                      <?php $i++;}?>
                                        </tbody>
                                    </table>
                                </div>
                                
                                
                                </div>
                                </div>
                                    
                                </div>
                            </div>
                    
                          
                            
                          
                          
                            
                        
                          
                          
                                       
                                    </div>
                                </div>
                            </div>
                 </div>
                
            <!--END CONTENT--><!--BEGIN FOOTER-->
            
           
        <!--END PAGE WRAPPER-->
           
           
           
                   
                  <?php 


$this->load->view('footer');

?>
 
                
            <!--END CONTENT--><!--BEGIN FOOTER-->
         
            </div>
            
         
</body>
</html>