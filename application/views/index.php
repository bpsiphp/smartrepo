<?php 
$url_old='core/login.php?entity=';
//$url_new='http://test.smartcookie.in/Welcome/login/';
$url_new=base_url('Clogin/login/');

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>:: Smart Cookie -  Student/Teacher Rewards Program ::</title>
<link href="<?php echo base_url();?>Assets/vendors/bootstrap/css/bootstrap.css"rel="stylesheet">
<script src="<?php echo base_url();?>Assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>Assets/vendors/bootstrap/js/bootstrap.min.js"></script>

<link href="<?php echo base_url();?>Assets/css/sc_style2.css"rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<style>
body{
	background-color:#fff;
}
.aln{
    display: block;
    margin-left: auto;
    margin-right: auto;
}
</style>
</head>
<body>
<div class="row1 header  bg-wht"> 
	<div class='container'>
		<div class="row " style="padding-top:20px;" >			
            <div class="col-md-7 visible-sm visible-lg visible-md">
                <img src="<?php echo base_url();?>images/250_86.png" />
            </div>
            <div class="col-md-7 visible-xs">
                <img src="<?php echo base_url();?>images/220_76.png" />
            </div>
			<div class='col-md-2'>
				<a class="btn btn-primary" href="core/express_registration_sp.php" >Registration</a> 
			</div>
			<div class="col-md-3" >	
				<div class="btn-group">
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Login <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
						<li><a href="<?php echo $url_new.'/student'; ?>">Student</a></li>
						<li><a href="<?php echo $url_old.'2'; ?>">Teacher</a></li>
						<li><a href="<?php echo $url_new.'/sponsor'; ?>">Sponsor</a></li>
						<li><a href="<?php echo $url_old.'5'; ?>">Parent</a></li>
						<li><a href="<?php echo $url_old.'1'; ?>">School Admin</a></li>
						<li><a href="<?php echo $url_old.'7'; ?>">School Staff</a></li>
						<li><a href="<?php echo $url_old.'6'; ?>">Cookie Admin</a></li>
						<li><a href="<?php echo $url_old.'8'; ?>">Cookie Staff</a></li>
						<li><a href="<?php echo $url_new.'/salesperson'; ?>">Sales Person</a></li>
				  </ul>
				</div>					
			</div>     
		</div>
	</div>
</div>
<div class="row2  bg-wht">         
	<div class="container "> 
		<div class="row">                                              
			<div class="col-md-12  text-center aln" style="padding-top:25px;" >
				<img src="<?php echo base_url();?>images/signin-imgbg.jpg" class="img-responsive"/>
			</div>
		</div>
	</div>
</div>  
<div class="row" style="background-color:#FFFFFF;padding-top:10px;" >
</div> 
<div class="row4 ">   
	<div class=" col-md-12 text-center footer2txt" >
		<a href="core/about-us.php">About us</a> |  
		<a href="core/contact-us.php">Contact Us</a> |  
		<a href="core/SmartCookie.pdf" target='_blank'>Info</a>  <br>                                   
		<a href="core/student.php">Students</a> |  
		<a href="core/college.php">School/College </a> |  
		<a href="core/teacher.php">Teachers</a> |  
		<a href="core/parent.php">Parents</a> |  
		<a href="core/sponsor.php">Vendors/Sponsors </a>
	</div>
</div>


<div>
	<div class=" col-md-12 text-center footer3txt" >
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<img src="<?php echo base_url();?>images/playstore.png" class="img-responsive"/>
				</div>
				<div class="col-md-4">
					<a href="http://goo.gl/vt1feP"><img src="<?php echo base_url();?>images/play_smartstudent.png"  title="Smart Student" height="64" width="64"></a>
					<!-- <a href="#"><img src="<?php echo base_url();?>images/play_smartstudentcoordinator.png"  title="Smart Student Coordinator" height="64" width="64"></a> -->
					<a href="http://goo.gl/IwXj8e"><img src="<?php echo base_url();?>images/play_smartteacher.png"  title="Smart Teacher" height="64" width="64"></a>
					<a href="http://goo.gl/KtV6Ia"><img src="<?php echo base_url();?>images/play_smartparent.png" title="Smart Parent" height="64" width="64"></a>
					<a href="http://goo.gl/GqH5XG"><img src="<?php echo base_url();?>images/play_smartsponsor.png"  title="Smart Sponsor" height="64" width="64"></a>
				</div>

				<div class="col-md-2">
					<img src="<?php echo base_url();?>images/appstore.png" class="img-responsive"/>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="<?php echo base_url();?>images/app_smartstudent.png"  title="Smart Student" height="64" width="64"></a>
					<!-- <a href="#"><img src="<?php echo base_url();?>images/app_smartstudentcoordinator.png"  title="Smart Student Coordinator" height="64" width="64"></a> -->
					<a href="#"><img src="<?php echo base_url();?>images/app_smartteacher.png"  title="Smart Teacher" height="64" width="64"></a>
					<a href="#"><img src="<?php echo base_url();?>images/app_smartparent.png" title="Smart Parent" height="64" width="64"></a>
					<a href="#"><img src="<?php echo base_url();?>images/app_smartsponsor.png"  title="Smart Sponsor" height="64" width="64"></a>
				</div>

								
			</div>
		</div>
	</div>
</div>


<div class=" row5">                                   
	<div class=" col-md-12 text-center footer3txt" >
		<div class="container "> 
			<div class="row">     
			Blue Planet Info Solutions © <?php echo date('Y',time());?> : 
			<a href="#">User Agreement </a> |  
			<a href="<?php echo base_url('core/privacypolicy.php');?>">Privacy Policy</a> |               
			<a href="#">Cookie Policy</a> |  
			<a href="#"> Copyright Policy </a>   
			</div>                        
		</div>                       
	</div>
</div>              
                
        
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Open+Sans::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); 
</script>
</body>
</html> 