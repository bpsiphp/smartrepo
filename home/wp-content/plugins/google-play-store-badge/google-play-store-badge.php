<?php
/*
Plugin Name: Google Play Store Badge
Plugin  URI: http://wordpress.org/plugins/playstore-badge
Description: A plugin to display using shortcodes the Google PlayStore Badge
Version: 1.0.2Author: Deniz Celebi & PixelartAuthor URI: http://profiles.wordpress.org/pixelart-dev/License: GPLv3License URI: http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
    Copyright 2014 Pixelart and Deniz Celebi  (email : office@pixelartdev.com)    This program is free software; you can redistribute it and/or modify    it under the terms of the GNU General Public License, version 2, as     published by the Free Software Foundation.    This program is distributed in the hope that it will be useful,    but WITHOUT ANY WARRANTY; without even the implied warranty of    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    GNU General Public License for more details.    You should have received a copy of the GNU General Public License    along with this program; if not, write to the Free Software    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA**/
global $allok;
$allok = true;
// Add Shortcode
function px_gp_shortcode($atts) {    global $allok;
	
	// Attributes
	extract( shortcode_atts(
		array(
			'lang' => 'en',
			'pkg'  => 'com.example.app',
			'size' => 'small',
			'type' => '1',
		), $atts));
		
		if(isset($lang) && isset($pkg) && isset($size) && isset($type)) {
		  $asize = px_size($size);
		  $atype = px_type($type);
		  $alang = $lang;
		  if($allok == true){
		    return '<a href="https://play.google.com/store/apps/details?id='.$pkg.'">
                   <img alt="Get it on Google Play" src="https://developer.android.com/images/brand/'.$alang.'_'.$atype.'_rgb_wo_'.$asize.'.png" />				</a>';
		  } else{
		    return '<p>Error in your shortcode!</p>';
		  }
	   } else {
	     return '<p>Please enter all parameters!</p>';
	   }
}
// Return the size
 function px_size($size){
    global $allok;
	
    if($size == 'small'){
	 return '45';
    } elseif($size == 'large'){
	 return '60';
    } else{
	 $allok = false;
    }
 }
 // Returns the type
 function px_type($type){
    global $allok;
    if($type == '1'){
	    return 'generic';
    } elseif($type == '2'){
		return 'app';
    } else{       $allok = false;
   }
 }
 
 // Add link on plugin page
 function px_plugin_help_link($links) { 
  $plugins_url = plugins_url();
  $help_link = '<a href="'.$plugins_url.'/playstore-badge/help.html">How to use</a>'; 
  array_unshift($links, $help_link); 
  return $links; 
 }
 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'px_plugin_help_link' );
add_filter('widget_text', 'do_shortcode');
add_shortcode( 'playbtn', 'px_gp_shortcode' );
?>