=== Google Play Store Badge ===
Contributors: pixelart-dev
Plugin  URI: http://wordpress.org/plugins/google-play-store-badge
Tags: google,play,store,badge,android,pixelartdev, gpsb, playstore, android, market,
Donation Link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2YCTDL7AFRHHG
Requires at least: 3.5
Tested up to: 4.0
Stable tag: 1.0.2
License: GPLv3
License URI: http://www.gnu.org/licenses/old-licenses/gpl-3.0.html

A plugin to display using shortcodes the Google PlayStore Badge

== Description ==
This plugin can be used to display in a simple way the Google Play Store Badge.
It can be put in a widget or as normal in a page or post. It is very simple to use and lightweight

= How to use it =
It is very very simple:
First you will need the shortcode: [playbtn]
This shortcode needs the following parameters to work:
 lang = The language in which the button should be displayed (all possible languages can be found at <http://developer.android.com/distribute/tools/promote/badges.html>),
 pkg = your App packagename,
 size = The size of the Badge (possible are small and large),
 type = The type of the Badge (possible are 1 and 2) where type 1 is generic that means the "get it on"-Badge. Type 2 is the app one which returns the "android app on"-Badge.,

A sample shortcode would look like this:
[playbtn lang="en" pkg="com.pixelart.odl" size="small" type="1"]

= Important: the plugin does only work if all parameters are given =


== Installation ==
1. Upload the folder to the wp-content/plugins/ directory on your server,
2. Activate the plugin,
3. Now you can place the shortcode where ever you want!


== Changelog ==
* 1.0.2 Fixed: Error in shortcode message (thanks to yoramzara),
* 1.0.1 Public Release,