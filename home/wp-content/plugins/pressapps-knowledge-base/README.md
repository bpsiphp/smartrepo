# pressapps-knowledge-base
## Action Hooks
`pakb_archive_loop` - action for archive page inside the loop

`pakb_category_loop` - action for category page inside the loop

`pakb_search_loop` - action for search inside the loop

`pakb_single_loop` - action for single page inside the loop