<?php
	include("sponsor_header.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SmartCookie</title>


  <link href="css/style.css" rel="stylesheet">
</head>

<body style="background-color:#EBEBEB;">
<div align="center">
    <div style="width:1002px; padding:10px 10px 10px 10px;">
    
    	<div style="height:50px; background-color:#FFFFFF; border:1px solid #CCCCCC;" align="left">
        	<h1 style="padding-left:20px; margin-top:2px;">Sponsorship</h1>
        </div>
        
        <div style="height:20px;"></div>
        <div style=" background-color:#FFFFFF; border:1px solid #CCCCCC;">
        <div style="padding:10px 10px 10px 10px; height:375px;" align="left">
        	<!--div for amount-->
             <div class="container" style="float:left; margin-right:10px; padding:5px 5px 5px 5px;">
             	<div style="border:1px solid #CCCCCC; width:300px;border: solid 1px gainsboro; transition: box-shadow 0.3s, border 0.3s; box-shadow: 0 0 5px 1px #969696;">
                	
                  <section  class="input-list style-1 clearfix">
                  	<h2 align="center">Sponsor Money</h2>
                    <h3 style="padding-left:10px;">Amount</h3>
                    
                        <input type="text" placeholder=":Amount in Doller">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                    <h3 style="padding-left:10px;">Points Per one Dollar</h3>
                    
                        <input type="text" placeholder=":Points per one Doller">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                      <div style="padding_left:30px;">
                        <input type="submit" Value="Submit" style="width:100px; height:40px; background-color:#0063C6; color:#FFFFFF; border:1px solid #CCCCCC;">
                      </div>
                  </section>
                 </div>
              </div>
              <!--div for product-->
               <div class="container" style="float:left; margin-right:10px; padding:5px 5px 5px 5px;">
               	 <form method="post" action="product.php">
                 <div style="border:1px solid #CCCCCC; width:300px;border: solid 1px gainsboro; transition: box-shadow 0.3s, border 0.3s; box-shadow: 0 0 5px 1px #969696;">
                 
                  <section  class="input-list style-1 clearfix">
                  	<h2 align="center">Sponsor Product</h2>
                    <h3 style="padding-left:10px;">Product name</h3>
                    
                        <input type="text" name="Sponser_product" placeholder=":Enter Prduct Name">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                    <h3 style="padding-left:10px;">Enter Product Name</h3>
                    
                        <input type="text" name="points_per_product" placeholder=":Number of Dollor per Product">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                      <div style="padding_left:30px;">
                        <input type="submit" Value="Submit" name="product" style="width:100px; height:40px; background-color:#0063C6; color:#FFFFFF; border:1px solid #CCCCCC;">
                      </div>
                  </section>
                 </div>
                 </form>
              </div>
              
              <!--div for discount-->
               <div class="container" style="padding:5px 5px 5px 5px; float:left;">
               	  <div style="border:1px solid #CCCCCC;border: solid 1px gainsboro; transition: box-shadow 0.3s, border 0.3s; box-shadow: 0 0 5px 1px #969696;">
                  <section  class="input-list style-1 clearfix">
                  	<h2 align="center">Sponsor Discount</h2>
                    <h3 style="padding-left:10px;">Product name</h3>
                    
                        <input type="text" placeholder=":Amount in Doller">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                    <h3 style="padding-left:10px;">Discount Per 100 Doller</h3>
                    
                        <input type="text" placeholder=":Discount Per 100 Doller">
                    
                  </section>
                  <section  class="input-list style-1 clearfix">
                      <div style="padding_left:30px;">
                        <input type="submit" Value="Submit" style="width:100px; height:40px; background-color:#0063C6; color:#FFFFFF; border:1px solid #CCCCCC;">
                      </div>
                  </section>
                  </div>
              </div>
        </div>
        
         <div style="height:20px;"></div>
        <!--List of already sponsored-->
        <div style="background-color:#FFFFFF; border:1px solid #FFFFFF;">
        <div style="height:30px;">
        	<h2>Already Sponsored</h2>
        </div>
        <table width="98%">
        	<tr style="background-color:#379BFF; color:#FFFFFF;">
            	<th>Sr. No.</th>
                
                <th>Product Name</th>
                <th>Points</th>
                <th>Number Of Coupans</th>
            </tr>
            <tr align="center">
            	<td>1</td>
            	
                <td>Cookies</td>
                <td>for 100 Points</td>
                <td>10</td>
                
            </tr>
        </table>
        
        </div>
        <div align="right" style="padding-right:30px;">
        <p>Powered by <a href="smartcookie.bpsi.us" style="text-decoration:none;">Smartcookie</a></p>
        </div>
       </div>
    </div>
</div>
</body>
</html>
